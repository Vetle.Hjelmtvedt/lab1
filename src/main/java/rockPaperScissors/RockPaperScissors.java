package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    // Random generator
    private Random rand = new Random();
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true) {
            System.out.println(String.format("Let's play round %d", roundCounter));
            
            // Fetch choice from user
            String humanChoice = getUserChoice();

            // Fetch random choice from "computer"
            String computerChoice = getComputerChoice();

            // Result string
            String resString = String.format("Human chose %s, computer chose %s.", humanChoice, computerChoice);

            if (is_winner(humanChoice, computerChoice)) {
                System.out.println(resString + " Human wins!");
                humanScore += 1;
            } else if (is_winner(computerChoice, humanChoice)) {
                System.out.println(resString + " Computer wins!");
                computerScore += 1;
            } else {
                System.out.println(resString + " It's a tie.");
            }
            System.out.println(String.format("Score: human %d, computer %d", humanScore, computerScore));

            boolean continuePlaying = continueAnswer();

            if (!continuePlaying) {
                break;
            }

            // Increase round score
            roundCounter += 1;
        }

        System.out.println("Bye bye :)");
    }

    public boolean validateInput(String input, List<String> validInput) {
        return validInput.contains(input.toLowerCase());

    }

    public boolean continueAnswer() {
        while (true) {
            String answer = readInput("Do you wish to continue playing? (y/n)?");
            if (validateInput(answer, List.of("y", "n"))) {
                return answer.equals("y");
            } else {
                System.out.println(String.format("I do not understand %s. Could you try again?", answer));
            }
        }
    }

    /**
     * Determines winner based on the given choices
     * @param choice1 - choice of actor 1
     * @param choice2 - choice of actor 2
     * @return - true if choice1 wins, false otherwise
     */
    public boolean is_winner(String choice1, String choice2) {
        if (choice1.equals("paper")) {
            return choice2.equals("rock");
        } else if (choice1.equals("scissors")) {
            return choice2.equals("paper");
        } else {
            return choice2.equals("scissors");
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();

        return userInput;
    }

    /**
     * Gets choice from user
     * @return user input/choice
     */
    public String getUserChoice() {
        while (true) {
            String choice = readInput("Your choice (Rock/Paper/Scissors)?");
            if (validateInput(choice, rpsChoices)) {
                return choice;
            } else {
                System.out.println(String.format("I do not understand %s. Could you try again?", choice));
            }
        }
    }

    /**
     * Generates random rock, paper, scissors choice
     * @return rps choice
     */
    public String getComputerChoice() {
        // Random index
        int randIndex = rand.nextInt(rpsChoices.size());
        // Return random choice
        return rpsChoices.get(randIndex);
    }

}
